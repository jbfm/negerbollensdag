<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Negerbollens dag <?= date('n') > 5 ? date('Y', strtotime('next year')) : date('Y')  ?></title>
		<meta name="description" content="Det har alltid hetat negerboll! Men nu är det <?= date('Y') ?> - Nu firar vi Chokladbollens dag">
		<meta property="og:image" content="http://negerbollensdag.se/img/og-boll.jpg">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="Shortcut Icon" href="img/favicon.png">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
		<link href='css/choklad.css' rel='stylesheet' type='text/css'>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-8628914-16', 'auto');
		  ga('send', 'pageview');

		</script>
	</head>
	<body>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<div class="background"></div>

		<article class="content hero">
			<div class="fb-share-button" data-href="http://negerbollensdag.se/" data-layout="button_count"></div>

			<div class="inner">
				<h1><span class="strikethrough">Neger</span>Chokladbollens dag</h1>
				<h2><time datetime="<?= date('n') > 5 ? date('Y', strtotime('next year')) : date('Y')  ?>-05-11">11e maj <?= date('n') > 5 ? date('Y', strtotime('next year')) : date('Y')  ?></time></h2>

				<div class="ad">
					<!-- Negerbollens dag topp ny -->
					<ins class="adsbygoogle"
					     style="display:block"
					     data-ad-client="ca-pub-2917774909243654"
					     data-ad-slot="3708022148"
					     data-ad-format="auto"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
			</div>
		</article>

		<div class="sources">
			<article class="content" id="sources">
				<div class="quote">
					<div class="quote-content text">Bakverket har även kallats negerboll, men då ordet "neger" kommit att uppfattas som nedsättande rekommenderar Språkrådet och Svenska Akademien att inte benämna bakverket så.</div>
					<a target="_blank" href="http://sv.wikipedia.org/wiki/Chokladboll" class="source">Wikipedia</a>
				</div>

				<div class="quote">
					<div class="quote-content video"><iframe src="//player.vimeo.com/video/42835392" width="940" frameborder="0" height="528" allowfullscreen></iframe></div>
					<a target="_blank" href="http://crazypictures.se/videos/negerboll?scrollpos=1838" class="source">Crazy Pictures</a>
				</div>

				<div class="ad">
					<!-- Negerbollens dag bottom -->
					<ins class="adsbygoogle"
					     style="display:block"
					     data-ad-client="ca-pub-2917774909243654"
					     data-ad-slot="9754555743"
					     data-ad-format="auto"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
			</article>
		</div>

		<div class="recipe" id="recept">
			<article class="content">
				<h1>Recept på fantastiska Chokladbollar</h1>
				<div class="ingredients">
					<h3>Ingredienser</h3>
					<ul>
						<li>100 g smör</li>
						<li>1 dl socker</li>
						<li>1 msk vaniljsocker</li>
						<li>3 msk kakao</li>
						<li>3 dl havregryn</li>
						<li>3 msk kallt starkt kaffe</li>
						<li>pärlsocker eller annan garnering</li>
					</ul>
				</div>
				<div class="procedure">
					<h3>Såhär gör du</h3>
					<ol>
						<li>Smält smöret och låt puttra en kort stund.</li>
						<li>Rör ihop socker, vaniljsocker, kakao, havregryn, kaffe och smöret. Ställ in i kylskåp så att smeten stelnar lite, ca 1 timme.</li>
						<li>Forma bollar. Rulla i pärlsocker (gärna mångfärgat), kokos eller annan garnering. Servera till kaffet.</li>
					</ol>

					<p>
						Receptet kommer från <a href="http://www.ica.se/recept/chokladbollar-5058/" target="_blank">ICA</a>.
					</p>
				</div>
			</article>
		</div>

		<article class="tasty" id="mums">
			<div class="preamble content">
				<h1>Andra goda Chokladbollar</h1>
				<p>
					Har du gjort <strong>chokladbollar</strong>? Fotografera och tagga med <em>#chokladboll</em> på <a href="http://instagram.com/" target="_blank">Instagram</a>, så kanske de syns här!
				</p>
			</div>
			<div class="instagrid">

			</div>
		</article>

		<div class="footer">
			<div class="content missing">
				<p>Tycker du något saknas? <a href="mailto:john@jmick.se">Maila mig</a>, så lägger jag in det!
				<br>Om du vill och kan, lägg till det själv, skicka en pull request på <a href="https://github.com/Lifesnoozer/negerbollensdag" target="_blank">GitHub</a>!</p>
			</div>
		</div>
		<script src="js/chokladboll.js"></script>
	</body>
</html>