(function() {
	var request = new XMLHttpRequest();

	var setUpGrid = function() {
		if (request.status !== 200) {
			return;
		}

		var data = JSON.parse(request.responseText).data;
		var docFrag = document.createDocumentFragment();
		for (var n in data) {
			if (data[n].type !== 'image') {
				continue;
			}

			var a = document.createElement('a');
			a.setAttribute('href', data[n].link);
			a.setAttribute('target', '_blank');
			a.className = 'instagram-image';

			var i = document.createElement('img');
			i.src = data[n].images.low_resolution.url;

			if (data[n].caption) {
				i.alt = data[n].caption.text;
			}

			i.width = data[n].images.low_resolution.width;
			i.height = data[n].images.low_resolution.height;

			a.appendChild(i);
			docFrag.appendChild(a);
		}

		document.querySelector('.instagrid').appendChild(docFrag);
	};

	request.overrideMimeType('appliaction/json');
	request.open('GET', 'ig.php', true);
	request.onload = setUpGrid;
	request.send(null);
})();