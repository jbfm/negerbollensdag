<?php
$cached = 'ig-cache.json';
$lastModDate = filemtime($cached);
if ( ! file_exists($cached) || ! $lastModDate || time() - $lastModDate > 60*60) {
	$data = file_get_contents('https://api.instagram.com/v1/tags/chokladboll/media/recent?callback=?&client_id=9e054dfb0787419cadeb46aa70a6ef2c');
	file_put_contents($cached, $data);
	echo $data;
} else {
	echo file_get_contents($cached);
}
